
# <img src=".ansible.png" alt="Ansible" style="width:26px;"/> Ansible

This repository is used for managing my infrastructure.

## Getting started
Installation is done with `pip` using Python 3 and venv. See the Links section for additional help installing Python 3.

Install git, direnv, python3 and pip3
```shell
$ sudo apt update && sudo apt install git direnv python3 python3-pip python3-setuptools
```

Clone this repository locally
```shell
$ git clone https://gitlab.com/doxic/ansible.git
$ cd ansible
```

Create a virtual environment
```shell
$ python3 -m venv .venv
```

Activate virtual environment.
```shell
$ source .venv/bin/activate
```

Allow direnv
```shell
direnv allow .
```

Add the following line at the end of the ~/.bashrc file:
```shell
eval "$(direnv hook bash)"
```

Or add the following line at the end of the ~/.zshrc file:
```shell
eval "$(direnv hook zsh)"
```

Install project dependencies
```shell
$ pip install -r requirements.txt
```

## Usage
### Create encrypted passwords for user module
Ansible ad-hoc command
```shell
ansible all -i localhost, -m debug -a "msg={{ devops_user_password | password_hash('sha512', \'$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)\') }}"
```


### Setup git-crypt
Configure a repository to use git-crypt (not required once initialized)
```shell
git-crypt init
```
List GPG keys
```shell
gpg --list-public-keys
```
Share the repository with others
```shell
git-crypt add-gpg-user --trusted 3884899842340B50
```
After cloning a repository with encrypted files, unlock with GPG
```shell
git-crypt unlock
```
List files for encryption
```shell
git-crypt status -e
```
### Ansible Vault
Set the diff driver for files
```shell
git config --global diff.ansible-vault.textconv "ansible-vault view"
```

```shell
# bash generate random 32 character alphanumeric string (upper and lowercase) and
cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1
```
### Create new Ansible role
Go to roles directory
```shell
cd roles
```
Initialise a new role with `molecule` command.
```shell
molecule init role --role-name common --verifier-name goss
```

### Systemd and Docker
systemd requires the kernel capability CAP_SYS_ADMIN

## Links
- Python
  - Installing Python 3 on Linux: https://docs.python-guide.org/starting/install3/linux/
  - How to Install Pip on Ubuntu 18.04: https://linuxize.com/post/how-to-install-pip-on-ubuntu-18.04/
  - Creation of virtual environments: https://docs.python.org/3/library/venv.html
- Molecule
  - Testing Ansible roles with molecule, goss and docker: http://linora-solutions.nl/post/testing_ansible_roles_with_molecule_goss_and_docker/
  - Goss - Quick and Easy server validation: https://github.com/aelsabbahy/goss
  - What is the scoop on running systemd in a container?:  https://developers.redhat.com/blog/2016/09/13/running-systemd-in-a-non-privileged-container/
  - Testing your Ansible roles with Molecule: https://www.jeffgeerling.com/blog/2018/testing-your-ansible-roles-molecule
- Ansible
  - Debug Ansible Playbooks Like A Pro: https://blog.codecentric.de/en/2017/06/debug-ansible-playbooks-like-pro/
  - DigitalOcean automation with Terraform and Ansible:  https://www.ottorask.com/blog/digitalocean-automation-with-terraform-and-ansible/
