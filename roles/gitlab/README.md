# Ansibel Role Gitlab

Gitlab role used to install and prepare gitlab for Demo purpuses
Do not use this in production!

## Manuall installation
Setup gitlab
```shell
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
sudo EXTERNAL_URL="http://vcs1-mgt-default.doxic.io" yum install -y gitlab-ce
sudo yum install -y gitlab-ce
```

seed the database
```shell
sudo mkdir /etc/gitlab/
sudo chmod 755 /etc/gitlab/

sudo touch /etc/gitlab/gitlab.rb
sudo chmod 600 /etc/gitlab/gitlab.rb

sudo vim /etc/gitlab/gitlab.rb
gitlab_rails['initial_root_password'] = 'xxx'
gitlab_rails['initial_shared_runners_registration_token'] = 'xxx'
```

## After installation
Create gitlab Personal Access Tokens
```shell
sudo gitlab-rails r "
token_digest = Gitlab::CryptoHelper.sha256 \"1234567890\";
token=PersonalAccessToken.create!(name: \"Full Access\", scopes: [:api], user: User.where(id: 1).first, token_digest: token_digest);
token.save!
";
```

# Create new user
```shell
curl -X POST \
  'http://vcs1-mgt-default.doxic.io/api/v4/users' \
  -H 'Private-Token: xxx' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{ "email": "test3@test.com",
  "username": "testname1",
  "password": "12345678",
  "name": "testname",
  "skip_confirmation": "true"
  }'
```

# create project
```shell
curl -X POST \
  'http://vcs1-mgt-default.doxic.io/api/v4/projects/user/4' \
  -H 'Private-Token: xxx' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "name": "my-project",
    "description": "my awesome project"
  }'
```


# Add deploy key
```shell
curl -X POST \
  'http://vcs1-mgt-default.doxic.io/api/v4/users/4/keys' \
  -H 'Private-Token: xxx' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d "{ \"title\": \"My deploy key\",
  \"key\": \"$(<~/.ssh/hetzner-doxic.id-rsa.pub)\"
  }"
```shell
GET /projects
curl \
  'http://vcs1-mgt-default.doxic.io/api/v4/projects' \
  -H 'Private-Token: xxx' | jq
```
## Links
- Seed the database (fresh installs only):  https://docs.gitlab.com/omnibus/settings/database.html#seed-the-database-fresh-installs-only
